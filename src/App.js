import React, { Component } from 'react';

import Menu from './components/menucomponent';
import Sort from './components/sort';
import { MOVIES } from './shared/MOVIES';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,Button, DropdownToggle, DropdownMenu, DropdownItem} from 'reactstrap';

import './App.css';
import logo from './logo.svg';




 
	
 class App extends Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      isOpen: false,
	   movie:MOVIES
	   
    };
  }
   toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }
  render() {
    return (
      <div class="bg-light">
        <Navbar color="dark" dark expand="md">
          <NavbarBrand href="/"><img src={logo} id="logo"/></NavbarBrand>
          <NavbarToggler onClick={this.toggle} />
          <Collapse isOpen={this.state.isOpen} navbar>
		  <Nav className="mr-auto" navbar>
              
              <NavItem>
                <NavLink href="#">DISOCVER</NavLink>
              </NavItem>
               <NavItem>
                <NavLink href="#">MOVIES</NavLink>
              </NavItem>
			   <NavItem>
                <NavLink href="#">TVSHOWS</NavLink>
              </NavItem>
            </Nav>
            <Nav className="ml-auto" navbar>
              
           
              <NavItem>
                <NavLink href="#">LOGIN</NavLink>
              </NavItem>
               <NavItem>
                <NavLink href="#">SIGNUP</NavLink>
              </NavItem>
            </Nav>
          </Collapse>
        </Navbar>
		    <div className="row">
             <div className="col-12 col-sm-12 col-ml-12 search ">
                  <span className="fa fa-search"></span>
                  <input  type="text" className="form-control " id="search" name="search" placeholder=' Search for a movie,tv show , person...'></input>
            </div><br/><br />
			
       </div>
	   
         <div className="col-12 col-sm-12 col-ml-12">
	    <div className=" row  ">
              
                <div className="offset-sm-1 col-9 col-sm-9 col-ml-9  "><h4>Popular Movies</h4></div>
               <div> <Button className="primary">Sort</Button>   </div>
	   
	
	   </div>
	   
	   
	   
	   
	   
	   </div>
	   <div>
		  < Menu movie={this.state.movie}/>
		  </div>
		  
      </div>
	  
	
	 
    );
  }
}
export default App;