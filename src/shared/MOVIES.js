export const MOVIES =
    [
        {
        id: 0,
        name:'Fightclub',
        image: 'assets/images/fightclub.jpe',
       
        
        rating:'6.2',
        review:'Fight Club is one movie that exactly caught the pre-millennial tension. Great performances, stunning visuals and a plot like nothing youve ever seen - one of the films of the year. ',
        actor:'Brad Pitt,Kevin Spacey,Morgan Freeman',
		date:'1999'
		},
		 {
        id: 1,
        name:'Taurus',
        image: 'assets/images/taurus.jpg',
       
       
        rating:'7.2',
        review:'Taurus is one movie that exactly caught the pre-millennial tension. Great performances, stunning visuals and a plot like nothing youve ever seen - one of the films of the year.',
        actor:'Mariya Kuznetsova,Sergei Razhu,Natalya Nikulenko,Lev Yeliseyev ,Nikolai Ustinov ',
		date:'2001'
		},
		 {
        id: 2,
        name:'The Silence of the Lambs',
        image: 'assets/images/thesilenceofthelambs.jpg',
       
        label:'Hot',
        rating:'9.1',
        review:'The Silence of the Lambs is one movie that exactly caught the pre-millennial tension. Great performances, stunning visuals and a plot like nothing youve ever seen - one of the films of the year. ',
        actor:'Jodie Foster,Lawrence A. Bonney,Lawrence A. Bonney,Scott Glenn',
		date:'2001'
		}
        ]