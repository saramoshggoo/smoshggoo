import React, { Component } from 'react';
import { Media } from 'reactstrap';

import { Card, CardImg, CardImgOverlay, CardText, CardBody,
    CardTitle } from 'reactstrap';

	
	
	
	
	
class Menu extends Component {
 
  constructor(props){
	  
	  super(props);
  }


 render() {
	    
        const menu = this.props.movie.map((movie) => {
            return (
		<div  className="col-12 col-sm-5 col-ml-5 m-4  rounded mb-0 bg-white" id="main" key={movie.id}>
			  
			  <div className="row ">
			      <div className="col-4 col-sm-4 col-ml-4 pl-0 "><img src={movie.image}  className="rounded" /></div>
                     <div className="col-7 col-sm-7 col-ml-7 ml-1 pt-2">
				         <div className="container  dont-break-out " >
					         <div className="row">
							       <div className="col-8 col-sm-8 col-ml-8  "  id="filmname"><b>{movie.name}</b></div>
					                <div className="col-4 col-sm-4 col-ml-4  text-right" ><b>{movie.rating}</b>&nbsp;<span className="fa fa-star"></span></div>
					          </div><br />
				              <div className="row text-primary" ><b className="text-secondary">ACTORS:</b> {movie.actor}</div><br/>
					          <div className="row" ><b>Year:</b> {movie.date}</div><br/>
					          <div className="row">
					             
                                   <div className='comments-space'><b>Reviews:&nbsp;</b>
								   
								   {movie.review}
								   
								   
								   
								   
								   </div>
								  
				    
					          </div>
					     </div>
					
				      
				 
				 
				      </div>
			   </div>
                
              </div>
            );
        });

        return (
            < div className="container">
                <div className="row">
                    {menu}
                </div>
                
            </div>
        );
    }
}
export default Menu;